<?php include('config.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" type="image/png" href="https://dog.ceo/img/favicon.png"/>

        <link href="public/bootstrap-5.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/assets/css/styles.css" rel="stylesheet">

        <?php $breed = $_GET['val']; ?>
        <title><?php echo ucwords($breed) ?> - Dogs Breeds</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a href="<?php echo BASE_URL ?>" class="navbar-brand">
                    <img src="https://dog.ceo/img/dog-api-logo.svg" height="40" alt="Logo">
                </a>
                <div class="navbar-brand mx-auto">
                    <h1>Dog Breeds</h1>
                </div>
            </div>
        </nav>
        <div class="separator"></div>

        <?php
        require_once 'autoload.php';
        use App\Classes\SubBreed;

        $dogs = new SubBreed();
        $response = $dogs->listSubBreeds($breed);

        echo "<div class='container'>";
        if ($response == false) {
            echo "<div class='error'>Error: Sub-Breed not found!</div>";
        } else {
            $images = $dogs->getSubBreedImages($breed, $response);
        
            $title = ucwords($breed);
            
            echo "<div class='' style='text-align: right;''><a class='btn btn-custom' href='" . BASE_URL . "'>Volver</a></div>";
            echo "<div class='row content'>";
            echo "<div class='title'>$title's Sub-Breeds</div>";

            foreach($response as $index) {
                $image = $images->$index;
                $ct = ucwords($index);
                
                echo "<div class='col-md-4 col-xs-12'>";
                echo "<div class='card'>";
                echo "<div class='img-gradient'>";
                echo "<img src='$image' alt='$index' height='300' width='100%'/>";
                echo "</div>";
                echo "<div class='card-body'>";
                echo "<h5 class='card-title' style='text-align: center;'>$ct</h5>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
            }
            echo "</div>";
        }
        echo "</div>";

        ?>

        <script src="public/bootstrap-5.1.3-dist/js/bootstrap.min.js"></script>
    </body>
</html>