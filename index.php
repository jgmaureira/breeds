<?php include('config.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" type="image/png" href="https://dog.ceo/img/favicon.png"/>

        <link href="public/bootstrap-5.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="public/assets/css/styles.css" rel="stylesheet">

        <title>Dogs Breeds</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a href="<?php echo BASE_URL ?>" class="navbar-brand">
                    <img src="https://dog.ceo/img/dog-api-logo.svg" height="40" alt="Logo">
                </a>
                <div class="navbar-brand mx-auto">
                    <h1>Dog Breeds</h1>
                </div>
            </div>
        </nav>
        <div class="separator"></div>

        <?php
        require_once 'autoload.php';
        use App\Classes\Breed;

        $dogs = new Breed();
        $response = $dogs->listBreeds();
        $images = $dogs->getBreedImages($response);

        $letter = '`';
        $first = true;
        echo "<div class='accordion accordion-flush' id='accordionFlush'>";
        foreach($response as $index => $value) {
            $image = $images->$index;
            $ct = ucwords($index);
            $count = count($value);
            $link = BASE_URL . "subbreed.php?val=$index";
            
            if ($letter == $index[0]) {
                echo "<div class='col-md-4 col-xs-12'>";
                    
                echo "<div class='card'>";
                if ($count > 0) {
                    echo "<a href='$link'>";
                    echo "<div class='img-gradient'>";
                    echo "<img src='$image' alt='$index' height='300' width='100%'/>";
                    echo "</div>";
                    echo "</a>";
                } else {
                    echo "<div class='img-gradient'>";
                    echo "<img src='$image' alt='$index' height='300' width='100%'/>";
                    echo "</div>";
                }

                if ($count > 0) {
                    echo "<a class='a' href='$link'>";
                    echo "<div class='card-body'>";
                    echo "<h5 class='card-title'>$ct ($count)</h5>";
                    echo "</div>";
                    echo '</a>';
                } else {
                    echo "<div class='card-body'>";
                    echo "<h5 class='card-title'>$ct</h5>";
                    echo "</div>";
                }

                echo "</div>";
                echo "</div>";
            } else {
                if (!$first) {
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                }                
        
                $letter = chr(ord($letter)+1);
        
                $expanded = $letter == 'a' ? true : false;
                $collapsed = $letter == 'a' ? '' : 'collapsed';
                $show = $letter == 'a' ? 'show' : '';
                
                if ($letter == $index[0]) {
                    echo "<div class='accordion-item'>";
                    echo "<h2 class='accordion-header' id='flush-heading-$letter'>";
                    echo "<button class='accordion-button $collapsed' type='button' data-bs-toggle='collapse' data-bs-target='#flush-collapse-$letter' aria-expanded='$expanded' aria-controls='flush-collapse-$letter'>";
                    $title = strtoupper($letter);
                    echo "$title";
                    echo "</button>";
                    echo "</h2>";
                    echo "<div id='flush-collapse-$letter' class='accordion-collapse collapse $show' aria-labelledby='flush-heading-$letter' data-bs-parent='#accordionFlush'>";
                    echo "<div class='accordion-body'>";
        
                    echo "<div class='container'>";
                    echo "<div class='row'>";
                    
                    echo "<div class='col-md-4 col-xs-12'>";
                    
                    echo "<div class='card'>";
                    if ($count > 0) {
                        echo "<a href='$link'>";
                        echo "<div class='img-gradient'>";
                        echo "<img src='$image' alt='$index' height='300' width='100%'/>";
                        echo "</div>";
                        echo "</a>";
                    } else {
                        echo "<div class='img-gradient'>";
                        echo "<img src='$image' alt='$index' height='300' width='100%'/>";
                        echo "</div>";
                    }

                    if ($count > 0) {
                        echo "<a class='a' href='$link'>";
                        echo "<div class='card-body'>";
                        echo "<h5 class='card-title'>$ct ($count)</h5>";
                        echo "</div>";
                        echo '</a>';
                    } else {
                        echo "<div class='card-body'>";
                        echo "<h5 class='card-title'>$ct</h5>";
                        echo "</div>";
                    }
                    
                    echo "</div>";
                    echo "</div>";
                    
                    $first = false;
                } else {
                    $first = true;
                }
            }
        }
        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</div>";

        ?>
    
        <script src="public/bootstrap-5.1.3-dist/js/bootstrap.min.js"></script>
    </body>
</html>