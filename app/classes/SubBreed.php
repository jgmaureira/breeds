<?php 

namespace App\Classes;

class SubBreed {
    public function listSubBreeds($breed) {
        $h = curl_init();
        $fetchURL = 'https://dog.ceo/api/breed/' . $breed . '/list';
        curl_setopt($h, CURLOPT_URL, $fetchURL);
        curl_setopt($h, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($h);
        $result = json_decode($result, true);
        $result = json_decode(json_encode($result));
        
        if ($result->status == 'error') {
            return false;
        } else {
            return $result->message;
        }
        
    }

    public function getSubBreedImages($breed, $array) {
        $multiCurl = array();
        $result = array();
        $mh = curl_multi_init();

        foreach($array as $index => $value) {
            $fetchURL = 'https://dog.ceo/api/breed/' . $breed . '/' . $value . '/images/random';
            $multiCurl[$value] = curl_init();
            curl_setopt($multiCurl[$value], CURLOPT_URL, $fetchURL);
            curl_setopt($multiCurl[$value], CURLOPT_HEADER,0);
            curl_setopt($multiCurl[$value], CURLOPT_RETURNTRANSFER,1);
            curl_multi_add_handle($mh, $multiCurl[$value]);
        }

        $idx = null;

        do {
            curl_multi_exec($mh, $idx);
        } while($idx > 0);

        foreach($multiCurl as $k => $ch) {
            $content = json_decode(curl_multi_getcontent($ch), true);
            $content = json_decode(json_encode($content));
            $result[$k] = $content->message;
            curl_multi_remove_handle($mh, $ch);
        }

        curl_multi_close($mh);

        return json_decode(json_encode($result));
    }
}